<?php 
   $db = new mysqli("localhost","umutceti_scandiweb","scandiweb..","umutceti_scandiweb") or die ("Disconnected");
   $db->set_charset("utf8");
   
   
   class system {
   
   	public static function getProducts($db) {
   		$product = "SELECT * FROM products";
   		$products = $db->prepare($product);
   		$products->execute();
   		$b=$products->get_result();
   		
   		while ($productlast=$b->fetch_assoc()) :	
   
   			if ($productlast["type"] == 1) {
   				$type = "DVD";
   
   				echo '<div class="col-md-3 product mb-3">
                  <div class="innerDiv">
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="card">
                              <div class="card-body">
                                 <div class="form-check">
                                    <input class="delete-checkbox form-check-input" name="delete[]" type="checkbox" value="'.$productlast["id"].'" id="product-'.$productlast["id"].'">
                                 </div>
                                 <p class="sku"><b>SKU : </b>'.$productlast["sku"].'</p>
                                 <p class="name"><b>Name : </b>'.$productlast["name"].'</p>
                                 <p class="price"><b>Price : </b>'.$productlast["price"].'</p>
                                 <p class="size"><b>Size : </b>'.$productlast["dvd_size"].'</p>
                                 <p class="size"><b>Type : </b> '.$type.'</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>';
   
   			} else if ($productlast["type"] == 2) {
   				$type = "Furniture";
   
   				echo '<div class="col-md-3 product mb-3">
                  <div class="innerDiv">
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="card">
                              <div class="card-body">
                                 <div class="form-check">
                                    <input class="delete-checkbox form-check-input" name="delete[]" type="checkbox" value="'.$productlast["id"].'" id="product-'.$productlast["id"].'">
                                 </div>
                                 <p class="sku"><b>SKU : </b>'.$productlast["sku"].'</p>
                                 <p class="name"><b>Name : </b>'.$productlast["name"].'</p>
                                 <p class="price"><b>Price : </b>'.$productlast["price"].'</p>
                                 <p class="size"><b>Dimension : </b>'.$productlast["furniture_h"].'X'.$productlast["furniture_w"].'X'.$productlast["furniture_l"].'</p>
                                 <p class="size"><b>Type : </b> '.$type.'</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>';
   
   			} else if ($productlast["type"] == 3) {
   				$type = "Book";

   				echo '<div class="col-md-3 product mb-3">
                  <div class="innerDiv">
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="card">
                              <div class="card-body">
                                 <div class="form-check">
                                    <input class="delete-checkbox form-check-input" name="delete[]" type="checkbox" value="'.$productlast["id"].'" id="product-'.$productlast["id"].'">
                                 </div>
                                 <p class="sku"><b>SKU : </b>'.$productlast["sku"].'</p>
                                 <p class="name"><b>Name : </b>'.$productlast["name"].'</p>
                                 <p class="price"><b>Price : </b>'.$productlast["price"].'</p>
                                 <p class="size"><b>Weight : </b>'.$productlast["book_w"].'</p>
                                 <p class="size"><b>Type : </b> '.$type.'</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>';
   			}
   		endwhile;
   	}     
   } 
    ?>