<?php 
   include("function.php");
?>
<!doctype html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- Bootstrap CSS -->
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
      <link rel="stylesheet" type="text/css" href="style.css">
      <title>Test Task</title>
   </head>
   <body>
      <form action="actions.php" method="POST" id="product_form">
         <!-- header of main section -->
         <div class="container-fluid customBorder px-5 mt-5">
            <div class="row mb-2">
               <div class="col-sm-9">
                  <h4>Product Add</h4>
               </div>
               <div class="col-sm-3 text-end">
                  <button id="save-product-btn" type="submit" onclick="checkSelectedType()" class="btn btn-success">Save</button>
                  <a href="index.php" id="cancel-product-btn" type="button" class="btn btn-danger">Cancel</a>
               </div>
            </div>
         </div>
         <!-- Product list -->
         <div class="container-fluid mt-5 addProduct">
            <div class="row">
               <div class="col-md-6">
                  <div class="row g-4 py-2 align-items-center">
                     <div class="col-md-2"><label for="sku" class="col-form-label">SKU</label></div>
                     <div class="col-md-10"><input required="required" type="text" name="sku" id="sku" class="form-control"></div>
                  </div>
                  <div class="row g-4  py-2 align-items-center">
                     <div class="col-md-2"><label for="name" class="col-form-label">Name</label></div>
                     <div class="col-md-10"><input required="required" type="text" name="name" id="name" class="form-control"></div>
                  </div>
                  <div class="row g-4 py-2 align-items-center">
                     <div class="col-md-2"><label for="price" class="col-form-label">Price ($)</label></div>
                     <div class="col-md-10"><input required="required" type="text" name="price" id="price" class="form-control"></div>
                  </div>
                  <div class="row g-4 py-2 align-items-center">
                     <div class="col-md-2"><label for="price" class="col-form-label">Type Switcher</label></div>
                     <div class="col-md-10">
                        <select  id="productType" name="type"  onchange="handleChange()" class="form-select">
                           <option value="0">- Select product type -</option>
                           <option value="1">DVD</option>
                           <option value="2">Furniture</option>
                           <option value="3">Book</option>
                        </select>
                     </div>
                  </div>
                  <div id="dvd" class="row g-4 py-2 align-items-center">
                     <p>Please provide size in MB format. -> <small>1.024 kb = 1 MB</small></p>
                     <div class="col-md-2"><label for="size" class="col-form-label">Size (MB)</label></div>
                     <div class="col-md-10"><input name="dvd_size" type="text" id="size" class="form-control"></div>
                  </div>
                  <div id="furniture" class="row g-4 py-2 align-items-center">
                     <p>Please provide dimensions in HxWxL format.</p>
                     <div class="col-md-2"><label for="height" class="col-form-label">Height (CM)</label></div>
                     <div class="col-md-10"><input name="furniture_h" type="text" id="height" class="form-control"></div>
                     <div class="col-md-2"><label for="width" class="col-form-label">Width (CM)</label></div>
                     <div class="col-md-10"><input name="furniture_w" type="text" id="width" class="form-control"></div>
                     <div class="col-md-2"><label for="length" class="col-form-label">Length (CM)</label></div>
                     <div class="col-md-10"><input name="furniture_l" type="text" id="length" class="form-control"></div>
                  </div>
                  <div id="book" class="row g-4 py-2 align-items-center">
                     <p>Please provide weight in KG format. -> <small>1.000 gr = 1 KG</small></p>
                     <div class="col-md-2"><label for="weight" class="col-form-label">Weight (KG)</label></div>
                     <div class="col-md-10"><input name="book_w" type="text" id="weight" class="form-control"></div>
                  </div>
               </div>
            </div>
         </div>
         <footer>
            <p class="copyright text-center mt-5">Test Assignment</p>
         </footer>
         <!-- Optional JavaScript;
            choose one of the two! --><!-- Option 1: Bootstrap Bundle with Popper --><script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script><!-- Option 2: Separate Popper and Bootstrap JS --><!-- <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script><script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>--><script type="text/javascript" src="custom.js" type=""></script>
      </form>
   </body>
</html>