<?php 

   /* add products */
   
       if (isset($_POST['name'], $_POST['price'], $_POST['sku'], $_POST['type'], $_POST['dvd_size'], $_POST['furniture_h'], $_POST['furniture_w'], $_POST['furniture_l'], $_POST['book_w'])) {
           $name = trim(filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING));
           $price = trim(filter_input(INPUT_POST, 'price', FILTER_SANITIZE_STRING));
           $sku = trim(filter_input(INPUT_POST, 'sku', FILTER_SANITIZE_STRING));
           $type = trim(filter_input(INPUT_POST, 'type', FILTER_SANITIZE_STRING));
           $dvd_size = trim(filter_input(INPUT_POST, 'dvd_size', FILTER_SANITIZE_STRING));
           $furniture_h = trim(filter_input(INPUT_POST, 'furniture_h', FILTER_SANITIZE_STRING));
           $furniture_w = trim(filter_input(INPUT_POST, 'furniture_w', FILTER_SANITIZE_STRING));
           $furniture_l = trim(filter_input(INPUT_POST, 'furniture_l', FILTER_SANITIZE_STRING));
           $book_w = trim(filter_input(INPUT_POST, 'book_w', FILTER_SANITIZE_STRING));
   
           if ($type == 1) {
              if (empty($name) || empty($price) || empty($sku) || empty($type) || empty($dvd_size)){
               die('<script> alert("Please, submit required data"); event.preventDefault(); </script>');
               } 
           } else if ($type == 2){
              if (empty($name) || empty($price) || empty($sku) || empty($type) || empty($furniture_h) || empty($furniture_w) || empty($furniture_l)){
               die('<script> alert("Please, submit required data"); event.preventDefault(); </script>');
               }
           } else if ($type == 3){
               if (empty($name) || empty($price) || empty($sku) || empty($type) || empty($book_w)){
               die('<script> alert("Please, submit required data"); event.preventDefault(); </script>');
               }
           } else {
              exit;
           }
       }
   
        try {
   
           $connect = new PDO("mysql:host=localhost;dbname=umutceti_scandiweb", "umutceti_scandiweb", "scandiweb..");
           $connect->exec("SET NAMES utf8");
           $connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   
   
           if ($type == 1) {
               $addProduct = $connect->prepare("INSERT INTO products(name,price,sku,type,dvd_size) VALUES(?,?,?,?,?)");
               $addProduct->bindParam(1, $name, PDO::PARAM_STR);
               $addProduct->bindParam(2, $price, PDO::PARAM_STR);
               $addProduct->bindParam(3, $sku, PDO::PARAM_STR);
               $addProduct->bindParam(4, $type, PDO::PARAM_STR);
               $addProduct->bindParam(5, $dvd_size, PDO::PARAM_STR);
           } else if ($type == 2){
               $addProduct = $connect->prepare("INSERT INTO products(name,price,sku,type,furniture_h,furniture_w,furniture_l) VALUES(?,?,?,?,?,?,?)");
               $addProduct->bindParam(1, $name, PDO::PARAM_STR);
               $addProduct->bindParam(2, $price, PDO::PARAM_STR);
               $addProduct->bindParam(3, $sku, PDO::PARAM_STR);
               $addProduct->bindParam(4, $type, PDO::PARAM_STR);
               $addProduct->bindParam(5, $furniture_h, PDO::PARAM_STR);
               $addProduct->bindParam(6, $furniture_w, PDO::PARAM_STR);
               $addProduct->bindParam(7, $furniture_l, PDO::PARAM_STR);
           } else if ($type == 3){
                $addProduct = $connect->prepare("INSERT INTO products(name,price,sku,type,book_w) VALUES(?,?,?,?,?)");
               $addProduct->bindParam(1, $name, PDO::PARAM_STR);
               $addProduct->bindParam(2, $price, PDO::PARAM_STR);
               $addProduct->bindParam(3, $sku, PDO::PARAM_STR);
               $addProduct->bindParam(4, $type, PDO::PARAM_STR);
               $addProduct->bindParam(5, $book_w, PDO::PARAM_STR);
           } else {
               exit;
           }
   
       $addProduct->execute();
       header("Location:index.php");
   
   } catch (PDOException $e) {
       die($e->getMessage());
   }
   $connect = null;
   
    ?>