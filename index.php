<?php 
   include("function.php");

   if ($count = $db->query("SELECT * FROM products")) {$row_count = $count->num_rows; };

    ?>
<!doctype html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- Bootstrap CSS -->
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
      <link rel="stylesheet" type="text/css" href="style.css">
      <title>Test Task</title>
   </head>
   <body>
      <form action="delete.php" method="POST" id="product_form">
         <!-- header of main section -->
         <div class="container-fluid customBorder px-5 mt-5">
            <div class="row mb-2">
               <div class="col-md-9">
                  <h4>Product List</h4>
               </div>
               <div class="col-md-3 text-end">
                  <a href="add-product" id="add-product-btn" type="button" class="btn btn-primary">ADD</a>
                  <?php 
                     if ($row_count == 0 ) {?>
                  <button id="delete-product-btn" disabled  type="button" class="btn btn-danger">MASS DELETE</button>
                  <?php } else {?>
                  <button id="delete-product-btn" onclick="checkboxControl()" name="deleteProduct" type="submit" class="btn btn-danger">MASS DELETE</button>
                  <?php } 
                     ?>

               </div>
            </div>
         </div>
         <!-- Product list -->
         
         <div class="container-fluid productList">
            <div class="row">
               
                  <?php 
                  if ($row_count == 0){?>
                  <strong>Hi! Please add a product.</strong>
                  <?php } ?>



                  <?php system::getProducts($db) ?>
            </div>
         </div>
         <footer>
            <p class="copyright text-center mt-5">Test Assignment</p>
         </footer>
         <!-- Optional JavaScript; choose one of the two! -->
         <!-- Option 1: Bootstrap Bundle with Popper -->
         <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
         <!-- Option 2: Separate Popper and Bootstrap JS -->
         <!--
            <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
            -->
         <script type="text/javascript" src="custom.js" type=""></script>

      </form>
   </body>
</html>